if [ -S "$SSH_AUTH_SOCK" ]; and [ ! -h "$SSH_AUTH_SOCK" ]
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
end
#fish_logo
alias pwgen="/bin/dd if=/dev/urandom bs=1 count=21 2>/dev/null | /usr/bin/base64 | /bin/sed -e 's#[+/O0I1l]##g'"


