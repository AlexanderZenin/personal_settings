set -x -g PAGER '/usr/bin/less'
alias ccat='pygmentize -g'
alias tmux='tmux -2'

set -x -g PATH /bin /sbin /usr/bin /usr/sbin /usr/local/bin

set -x -g PGPASSWORD 'Y5j4877$'
alias sql='psql -h rgm-t-torodb01.hq.root.ad -U torodb toro'

set fish_greeting

[ -f ~/.lastwd ]; and cd (cat ~/.lastwd)

function on_exit --on-process %self
    pwd > ~/.lastwd
end

