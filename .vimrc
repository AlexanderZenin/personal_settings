if strlen(finddir('~/.vim-tmp')) == 0
    silent execute '!mkdir ~/.vim-tmp'
endif
set directory=~/.vim-tmp

set nocompatible
set shell=/bin/bash
set number
filetype off

" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tmhedberg/matchit'
Plugin 'alvan/vim-closetag'
Plugin 'tpope/vim-fugitive'
" Plugin 'wincent/Command-T'
Plugin 'scrooloose/syntastic'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_theme="lucius"
Plugin 'flazz/vim-colorschemes'
Plugin 'mileszs/ack.vim'

" markdown highlighting
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'romainl/apprentice'
:let g:vim_markdown_folding_level = 3

call vundle#end()

filetype on
filetype plugin on
filetype indent on

set nohlsearch
set incsearch
set ignorecase
set smartcase

set autoindent
set t_Co=256
colorscheme apprentice "desert256 "dw_blue
syntax on
set number
set scrolloff=3
set wildmenu
set wildmode=list:longest
set hidden
set pastetoggle=<F2>
set mouse=nv
set mousemodel=extend

set encoding=utf-8

let s:tabwidth=4
exec 'set tabstop=' .s:tabwidth
exec 'set shiftwidth='.s:tabwidth
exec 'set softtabstop='.s:tabwidth
set shiftround
set expandtab
set bs=2

set laststatus=2
set noshowmode

command FixUnicode %s@\\u\(\x\{4\}\)@\=nr2char('0x'.submatch(1),1)@g

" https://www.linux.com/learn/vim-tips-folding-fun
" nice folding tips
"
if v:version > 703 || v:version == 703 && has('patch541')
    set formatoptions+=j 
endif

let mapleader="\<Space>"
nmap <Leader><Leader> V

:highlight ExtraWhitespace ctermbg=217 guibg=red
:match ExtraWhitespace /\s\+$/

" Highlight 80th column and 120+
let &colorcolumn="80,".join(range(120,999),",")
highlight ColorColumn ctermbg=237 guibg=#2c2d27

" Open new split panes to right and bottom, which feel more natural
set splitbelow
set splitright

nnoremap <Tab> w
nnoremap <S-Tab> :tabnext<CR>


" Задаем собственные функции для назначения имен заголовкам табов -->
    function MyTabLine()
        let tabline = ''

        " Формируем tabline для каждой вкладки -->
            for i in range(tabpagenr('$'))
                " Подсвечиваем заголовок выбранной в данный момент вкладки.
                if i + 1 == tabpagenr()
                    let tabline .= '%#TabLineSel#'
                else
                    let tabline .= '%#TabLine#'
                endif

                " Устанавливаем номер вкладки
                let tabline .= '%' . (i + 1) . 'T'

                " Получаем имя вкладки
                let tabline .= ' %{MyTabLabel(' . (i + 1) . ')} |'
            endfor
        " Формируем tabline для каждой вкладки <--

        " Заполняем лишнее пространство
        let tabline .= '%#TabLineFill#%T'

        " Выровненная по правому краю кнопка закрытия вкладки
        if tabpagenr('$') > 1
            let tabline .= '%=%#TabLine#%999XX'
        endif

        return tabline
    endfunction

    function MyTabLabel(n)
        let label = ''
        let buflist = tabpagebuflist(a:n)

        " Имя файла и номер вкладки -->
            let label = substitute(bufname(buflist[tabpagewinnr(a:n) - 1]), '.*/', '', '')

            if label == ''
                let label = '[No Name]'
            endif

            let label .= ' (' . a:n . ')'
        " Имя файла и номер вкладки <--

        " Определяем, есть ли во вкладке хотя бы один
        " модифицированный буфер.
        " -->
            for i in range(len(buflist))
                if getbufvar(buflist[i], "&modified")
                    let label = '[+] ' . label
                    break
                endif
            endfor
        " <--

        return label
    endfunction

    function MyGuiTabLabel()
        return '%{MyTabLabel(' . tabpagenr() . ')}'
    endfunction

    set tabline=%!MyTabLine()
    set guitablabel=%!MyGuiTabLabel()
" Задаем собственные функции для назначения имен заголовкам табов <--

